s=10;
rand("seed",s);
n=100;
U=rand(n,n);
n=3;
L=rand(n,n);
b= rand(n);

disp("-------------------------------usolve---------------------------------")
tic;
c1 = usolve(U,b);
toc
disp("-------------Vérification----------")

tic;
c11 = triu(U);
toc


disp("-------------------------------lsolve---------------------------------")
tic;
c2 = lsolve(L,b);
toc


disp("-------------Vérification----------")

tic;
c2 = tril(L);
toc
