function [x] = lsolve(L,b)
    n=size(b,1);
   // L = zeros(n,n);
    x(1)= b(1)/L(1,1);
    
    for i= 2 : n
        x(i)  = (b(i)-L(i,1 : (i-1))*x(1:(i-1)))/L(i,i);
        
    end
    
    
xex = rand(n,1);
d = L*xex;
x = L\d;
frelres = norm(x-xex)/norm(xex); //erreur avant
brelres = norm(d-L*x)/norm(d); //erreur arrière
c = cond(L);
born = c*(brelres);
disp("------------------affichage des erreur et autre pour lsolve----------------")
disp("b=  ",d)
disp("A =",L )
disp( "xex = ",xex )
disp("frelres = ",frelres  )
disp("frelres = ",frelres )
disp("brelres",brelres )
disp("Conditionnement =  ",c )
disp("born = ",born)


endfunction
