function [x] = usolve(U,b)
    n = size(b,1);
   // U =zeros(n,n);
    x(n) = b(n)/U(n,n)
    
    for i= n-1: -1 : 1
        x(i) = (b(i)-U(i,(i+2) :n)*x((i+1):n))/u(i,i);
        
    end

xex = rand(n,1);
d = U*xex;
x = U\d;
frelres = norm(x-xex)/norm(xex); //erreur avant
brelres = norm(d-U*x)/norm(d); //erreur arrière
c = cond(U);
born = c*(brelres);

disp("------------------affichage des erreur et autre pour usolve----------------")
disp("b=  ",d)
disp("A =",U )
disp( "xex = ",xex )
disp("frelres = ",frelres  )
disp("frelres = ",frelres )
disp("brelres",brelres )
disp("Conditionnement =  ",c )
disp("born = ",born)

endfunction
